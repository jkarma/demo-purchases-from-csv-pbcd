# demo-purchases-from-csv-pbcd

Demo project using jKarma for detecting changes on the customer purchases history, loaded by reading a CSV file. The problem is solved by means of a PBCD algorithm built on top of a customized pattern mining strategy.