/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.examples.demo_purchases_from_csv_pbcd;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.stream.Stream;

import org.jkarma.examples.purchases.boilerplate.Utils;
import org.jkarma.examples.purchases.model.Product;
import org.jkarma.examples.purchases.model.Purchase;
import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.providers.TidSetProvider;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Strategies;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.mining.windows.Windows;
import org.jkarma.pbcd.descriptors.Descriptors;
import org.jkarma.pbcd.detectors.Detectors;
import org.jkarma.pbcd.detectors.PBCD;
import org.jkarma.pbcd.events.ChangeDescriptionCompletedEvent;
import org.jkarma.pbcd.events.ChangeDescriptionStartedEvent;
import org.jkarma.pbcd.events.ChangeDetectedEvent;
import org.jkarma.pbcd.events.ChangeNotDetectedEvent;
import org.jkarma.pbcd.events.PBCDEventListener;
import org.jkarma.pbcd.events.PatternUpdateCompletedEvent;
import org.jkarma.pbcd.events.PatternUpdateStartedEvent;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.pbcd.similarities.UnweightedJaccard;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;

/**
 * An application showing how to define a PBCD an how to run it over a Stream<Purchase> 
 * instance. In this demo the stream is created by parsing the content of a CSV file, 
 * also the input parameters are passed as command line arguments. The application can 
 * be launched via command line interface by invoking this entry point with the arguments:
 * 
 *  "java -jar demo.jar -i dataset.csv -minFreq 0.2 -minChange 0.5 -blockSize 2"
 *  
 * an example of csv input file can be found in the root folder of this project and is
 * named as "purchase_history.csv". The format of the csv file should be the following
 * (header included).
 *  
 *  timestamp, products
 *  2019-10-01 00:00:00:0 -0000, "sugar, wine, bread"
 *  2019-10-02 00:00:00:0 -0000, "wine, bread"
 *  2019-10-03 00:00:00:0 -0000, "cake, bread"
 *  ...
 * 
 * @author Angelo Impedovo
 */
public class Demo {

	/**
	 * The number of transactions to be consumed together by the PBCD.
	 */
	@Option(required=true, name="-blockSize", usage="block size")
	public int blockSize = 2;
	
	
	/**
	 * The minimum frequency threshold.
	 */
	@Option(required=true, name="-minFreq", usage="minimum frequency threshold")
	public float minFreq = 0.2f;

	
	/**
	 * The minimum change threshold.
	 */
	@Option(required=true, name="-minChange", usage="minimum change threshold")
	public float minChange = 0.5f;

	
	/**
	 * The input file containing the dataset.
	 */
	@Option(required=true, name="-i", usage="input filename")
	public File inputFile = null;


	/**
	 * Builds a PBCD based on frequent combinations of foods and drinks
	 * in the blockwise sliding model. This PBCD computes the change score
	 * by means of a binary jaccard score, and explain changes by extracting
	 * the emerging patterns.
	 * @return the PBCD delegate.
	 */
	public PBCD<Purchase, Product, TidSet, Boolean> getPBCD(){
		//we prepare the time window model and the data accessor
		WindowingStrategy<TidSet> model = Windows.blockwiseSliding();
		TidSetProvider<Product> accessor = new TidSetProvider<>(model);

		//we instantiate the pattern language delegate
		MixedProductJoiner language = new MixedProductJoiner();

		//we instantiate the mining strategy
		MiningStrategy<Product, TidSet> strategy = Strategies
			.upon(language).eclat(this.minFreq).dfs(accessor);

		//we assemble the PBCD
		return Detectors.upon(strategy)
			.unweighted((p,t) -> Patterns.isFrequent(p, this.minFreq, t), new UnweightedJaccard())
			.describe(Descriptors.partialEps(this.minFreq, 1.00))
			.build(this.minChange, this.blockSize);
	}


	/**
	 * Returns a Stream<Purchase> sorted by timestamp, by reading the content
	 * of a CSV file.
	 * @return an in-memory stream of purchases.
	 */
	public Stream<Purchase> getDataset() throws FileNotFoundException{
		return Utils.parseStream(this.inputFile, Purchase.class)
			.sorted(Comparator.comparing(Purchase::getTimestamp));
	}



	/**
	 * Entry point of the application. 
	 * @param args
	 */
	public static void main(String[] args){
		final Demo demo = new Demo();
		final CmdLineParser argsParser = new CmdLineParser(demo);

		try {
			//we parse the command line arguments
			argsParser.parseArgument(args);

			//we get the dataset
			Stream<Purchase> dataset = demo.getDataset();
			PBCD<Purchase,Product,TidSet,Boolean> detector = demo.getPBCD();

			//we listen for change detection events
			detector.registerListener(
				new PBCDEventListener<Product,TidSet>() {

					@Override
					public void patternUpdateCompleted(PatternUpdateCompletedEvent<Product, TidSet> arg0) {
						//do nothing	
					}

					@Override
					public void patternUpdateStarted(PatternUpdateStartedEvent<Product, TidSet> arg0) {
						//do nothing			
					}

					@Override
					public void changeDetected(ChangeDetectedEvent<Product, TidSet> event) {
						//we show the change score
						System.out.println("change detected: "+event.getAmount());
						System.out.println("\tdescribed by:");
						
						//and the associated explanation
						event.getDescription().forEach(p -> {
							double freqReference = p.getFirstEval().getRelativeFrequency()*100;
							double freqTarget = p.getSecondEval().getRelativeFrequency()*100;

							String message;
							if(freqTarget > freqReference) {
								message="increased frequency from ";
							}else {
								message="decreased frequency from ";
							}
							message+=Double.toString(freqReference)+"% to "+Double.toString(freqTarget)+"%";
							System.out.println("\t\t"+p.getItemSet()+" "+message);
						});
					}

					@Override
					public void changeNotDetected(ChangeNotDetectedEvent<Product, TidSet> arg0) {
						//we show the change score only
						System.out.println("change not detected: "+arg0.getAmount());
					}

					@Override
					public void changeDescriptionCompleted(ChangeDescriptionCompletedEvent<Product, TidSet> arg0) {
						//do nothing
					}

					@Override
					public void changeDescriptionStarted(ChangeDescriptionStartedEvent<Product, TidSet> arg0) {
						//do nothing			
					}  

				}
			);

			//we listen for events
			dataset.forEach(p -> {
				System.out.println(p);
				detector.accept(p);
			});

		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
					argsParser.printExample(OptionHandlerFilter.ALL)
					);	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


}
