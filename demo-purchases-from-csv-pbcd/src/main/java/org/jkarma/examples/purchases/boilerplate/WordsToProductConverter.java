/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.examples.purchases.boilerplate;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import org.jkarma.examples.purchases.model.Product;

import com.univocity.parsers.conversions.Conversion;

/**
 * Boilerplate class which converts a multivalue field in a csv file as a set of
 * product. In particular a field encompassed by double quotes is taken and split
 * using a separator, then each token constitute the name of a new product.
 * This class also implements the opposite operation of serializing a set of products
 * into a string.
 * @author Angelo Impedovo
 *
 */
public class WordsToProductConverter implements Conversion<String, Set<Product>> {

	/**
	 * The separator to be considered when splitting tokens.
	 */
	private final String separator;

	public WordsToProductConverter(String... args) {
		String separator = ",";

		if (args.length == 1) {
			separator = args[0];
		}

		this.separator = separator;
	}

	public WordsToProductConverter(String separator) {
		this.separator = separator;
	}

	
	/**
	 * Converts a string into a set of products.
	 */
	public Set<Product> execute(String input) {
		if (input == null) {
			return Collections.emptySet();
		}

		Set<Product> out = new TreeSet<Product>();
		for (String token : input.split(separator)) {
			//extracting words separated by white space as well
			for (String word : token.trim().split("\\s")) {
				Product p = new Product(word.trim());
				out.add(p);
			}
		}

		return out;
	}

	
	/**
	 * Converts a set of products into a string.
	 */
	public String revert(Set<Product> input) {
		if (input == null || input.isEmpty()) {
			return null;
		}
		StringBuilder out = new StringBuilder();

		for (Product product : input) {
			if (product == null || product.getName().trim().isEmpty()) {
				continue;
			}
			if (out.length() > 0) {
				out.append(separator);
			}
			out.append(product.getName().trim());
		}

		if (out.length() == 0) {
			return null;
		}

		return out.toString();
	}
}
